// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  endpoint:'http://192.168.0.16:8080/tcv/',
  apiEndpointR:'http://localhost:8080/restaurants/',
  apiEndpointE:'http://localhost:8080/entreprises/',
  apiEndpointC:'http://localhost:8080/collaborateurs/',
  apiEndpointO:'http://localhost:8080/operations/',
  apiEndpointS:'http://localhost:8080/',
  items:0,
};


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StudentComponent} from '../professor/student/student.component';
import {BulletinComponent} from '../professor/bulletin/bulletin.component';
import {NoteComponent} from '../professor/note/note.component';
import {ProgrammeComponent} from '../professor/programme/programme.component';
import {ProfessorComponent} from '../professor/professor/professor.component';
import {ClasseComponent} from '../professor/classe/classe.component';
import {ClasseViewComponent} from '../professor/classe-view/classe-view.component';
import {TrajetComponent} from './Trajet/trajet.component';
import {ArretComponent} from './Arret/arret.component';
import {VoyageurComponent} from './Voyageur/voyageur.component';
import {BusComponent} from './Bus/bus.component';

const routes: Routes = [
  {
    path: 'busapp',
    data: {
      title: 'busapp'
    },

    children: [
      {
        path: 'trajets',
        component: TrajetComponent,
        data: {
          title: 'trajets'
        }
      },
      {
        path: 'arrets',
        component: ArretComponent,
        data: {
          title: 'arrets'
        }
      },
      {
        path: 'voyageurs',
        component: VoyageurComponent,
        data: {
          title: 'voyageurs'
        }
      },
      {
        path: 'bus',
        component: BusComponent,
        data:{
          title:'bus'
        }
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusAppRoutingModule { }

import {Component, Inject, OnInit, VERSION, ViewChild} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import {ArretService} from '../../../Services/arret.service';
import {Arret} from '../../../model/Arret';
import {Bus} from '../../../model/Bus';
import {BusService} from '../../../Services/bus.service';
import {Trajet} from '../../../model/Trajet';
import {TrajetService} from '../../../Services/trajet.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  arretList:Arret[]=[];
  busList=[];
  public breakpoint: number; // Breakpoint observer code
  public fname: string = `Ramesh`;
  public lname: string = `Suresh`;
  public addCusForm: FormGroup;
  wasFormChanged = false;
  selectedBusId:number = 0;
  selectedArretDeptId:number = 0;
  selectedArretArrId:number = 0;
  trajet:Trajet = new Trajet();
  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private arretService:ArretService,
    private busService:BusService,
    private trajetService:TrajetService,
    @Inject(MAT_DIALOG_DATA) public data: Trajet,
    public dialogRef: MatDialogRef<AddComponent>,
  ) { }

  public ngOnInit(): void {
    this.addCusForm = this.fb.group({
      IdProof: null,
      firstname: [this.fname, [Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*')]],
      lastname: [this.lname, [Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*')]],
      email: [null, [Validators.required, Validators.email]],
    });
    this.breakpoint = window.innerWidth <= 600 ? 1 : 2; // Breakpoint observer code

    this.arretService.getAllArret()
      .subscribe(data=>{
        this.arretList = data;
      });

    this.busService.getAllBus()
      .subscribe(data=>{
        this.busList=data;
      });
  }

  public onAddCus(): void {
    this.markAsDirty(this.addCusForm);
  }

  openDialog(): void {
    console.log(this.wasFormChanged);
    if(this.addCusForm.dirty) {
      // const dialogRef = this.dialog.open(DeleteComponent, {
      //   width: '340px',
      // });
    } else {
      this.dialog.closeAll();
    }
  }

  // tslint:disable-next-line:no-any
  public onResize(event: any): void {
    this.breakpoint = event.target.innerWidth <= 600 ? 1 : 2;
  }

  private markAsDirty(group: FormGroup): void {
    group.markAsDirty();
    // tslint:disable-next-line:forin
    for (const i in group.controls) {
      group.controls[i].markAsDirty();
    }
  }

  formChanged() {
    this.wasFormChanged = true;
  }
  saveTrajet(){
    if(this.selectedArretArrId!=0 && this.selectedArretDeptId!=0 && this.selectedBusId!=0){

      this.trajetService.createTrajet(this.selectedArretDeptId,this.selectedArretArrId,this.selectedBusId,this.trajet)
        .subscribe(data=>{
          console.log(data);
          this.dialogRef.close(data);

        })
    }
  }

}

import {Component, OnInit, ViewChild,ChangeDetectorRef } from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {AddComponent} from './add/add.component';
import {TrajetService} from '../../Services/trajet.service';
import {Trajet} from '../../model/Trajet';
export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

/** Constants used to fill up our data base. */
const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];
@Component({
  selector: 'app-trajet',
  templateUrl: './trajet.component.html',
  styleUrls: ['./trajet.component.scss']
})
export class TrajetComponent implements OnInit {
  trajetList:Trajet[]=[];
  displayedColumns: string[] = [ 'depart', 'arrivee', 'date-depart', 'date-arrivee','heure-depart','heure-arrivee'];
  dataSource: MatTableDataSource<Trajet>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog,public trajetService:TrajetService,private changeDetectorRefs: ChangeDetectorRef) {
    // Create 100 users
   // const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

    // Assign the data to the data source for the table to render
   // this.dataSource = new MatTableDataSource(users);

  }

  ngOnInit() {
    this.getAllTrajets();


  }

  private getAllTrajets() {
    this.trajetService.getAlltrajets()
      .subscribe(data => {
        this.trajetList = data;
        this.dataSource = new MatTableDataSource(this.trajetList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.dataSource.sort = this.sort;
        this.dataSource.data = this.trajetList;
      })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddComponent,{
      width: '640px',disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      // this.trajetList.push(result);
      // this.dataSource = new MatTableDataSource(this.trajetList);
      // this.changeDetectorRefs.detectChanges();
      // this.dataSource.paginator = this.paginator;
      // this.dataSource.sort = this.sort;
      // console.log(result);
      this.getAllTrajets();
    });
  }
  sortingDataAccessor(item, property) {
    // if (property.includes('.')) {
    //   return property.split('.')
    //     .reduce((object, key) => object[key], item);
    // }
    // return item[property];
    switch(property) {
      case 'depart': return item.arretDepart.nom;
      case 'arrivee': return item.arretArrivee.nom;
      case 'date-depart': return item.dateDepart;
      case 'date-arrivee': return item.dateArrivee;
      case 'heure-depart': return item.heureDepart;
      case 'heure-arrivee': return item.heureArrivee;
      default: return item[property];
    }
  }
}


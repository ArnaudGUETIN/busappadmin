import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {AddarretComponent} from './addarret/addarret.component';
import {Arret} from '../../model/Arret';
import {ArretService} from '../../Services/arret.service';
//import {AddComponent} from './add/add.component';


export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

/** Constants used to fill up our data base. */
const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];
@Component({
  selector: 'app-arret',
  templateUrl: './arret.component.html',
  styleUrls: ['./arret.component.scss']
})
export class ArretComponent implements OnInit {


  displayedColumns: string[] = ['nom', 'longitude', 'latitude', 'ville'];
  dataSource: MatTableDataSource<Arret>;
  arretsList:Arret[]=[];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog,private arretService:ArretService,private changeDetectorRefs: ChangeDetectorRef) {
    // Create 100 users
    // const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));
    //
    // // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(users);
  }

  ngOnInit() {
    this.getAllArrets();
  }

  private getAllArrets() {
    this.arretService.getAllArret()
      .subscribe(data => {
        this.arretsList = data;
        this.dataSource = new MatTableDataSource(this.arretsList);
        this.dataSource.sortingDataAccessor=this.sortingDataAccessor;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(AddarretComponent,{
      width: '640px',disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      // this.arretsList.push(result);
      // this.dataSource = new MatTableDataSource(this.arretsList);
      // this.changeDetectorRefs.detectChanges();
      // this.dataSource.paginator = this.paginator;
      // this.dataSource.sort = this.sort;
      this.getAllArrets();

    });
  }
  sortingDataAccessor(item, property) {
    // if (property.includes('.')) {
    //   return property.split('.')
    //     .reduce((object, key) => object[key], item);
    // }
    // return item[property];
    switch(property) {
      case 'ville': return item.nomVille;
      default: return item[property];
    }
  }


}

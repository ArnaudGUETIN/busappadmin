import {Component, Inject, OnInit, VERSION, ViewChild} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import {ArretService} from '../../../Services/arret.service';
import {Arret} from '../../../model/Arret';
import {Ville} from '../../../model/Ville';
import {VilleService} from '../../../Services/ville.service';
import {Trajet} from '../../../model/Trajet';

@Component({
  selector: 'app-addarret',
  templateUrl: './addarret.component.html',
  styleUrls: ['./addarret.component.scss']
})
export class AddarretComponent implements OnInit {

  public breakpoint: number; // Breakpoint observer code
  public fname: string = `Ramesh`;
  public lname: string = `Suresh`;
  public addCusForm: FormGroup;
  public selectedVilleId:number=0;
  arret:Arret =new Arret();
  wasFormChanged = false;
  arretList:Arret[]=[];
  villeList:Ville[]=[];
  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private arretService:ArretService,
    private villeService:VilleService,
    @Inject(MAT_DIALOG_DATA) public data: Arret,
    public dialogRef: MatDialogRef<AddarretComponent>,
  ) { }

  public ngOnInit(): void {
    this.addCusForm = this.fb.group({
      IdProof: null,
      firstname: [this.fname, [Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*')]],
      lastname: [this.lname, [Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*')]],
      email: [null, [Validators.required, Validators.email]],
    });
    this.breakpoint = window.innerWidth <= 600 ? 1 : 2; // Breakpoint observer code

    this.arretService.getAllArret()
      .subscribe(data=>{
        this.arretList = data;
      })

    this.villeService.getAllVille()
      .subscribe(data=>{
        this.villeList =  data;
      })
  }

  public onAddCus(): void {
    this.markAsDirty(this.addCusForm);
  }

  openDialog(): void {
    console.log(this.wasFormChanged);
    if(this.addCusForm.dirty) {
      // const dialogRef = this.dialog.open(DeleteComponent, {
      //   width: '340px',
      // });
    } else {
      this.dialog.closeAll();
    }
  }

  // tslint:disable-next-line:no-any
  public onResize(event: any): void {
    this.breakpoint = event.target.innerWidth <= 600 ? 1 : 2;
  }

  private markAsDirty(group: FormGroup): void {
    group.markAsDirty();
    // tslint:disable-next-line:forin
    for (const i in group.controls) {
      group.controls[i].markAsDirty();
    }
  }

  formChanged() {
    this.wasFormChanged = true;
  }

  saveArret(){
    this.arretService.createArret(this.selectedVilleId,this.arret)
      .subscribe(data=>{
        this.dialogRef.close(data);
      })
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddarretComponent } from './addarret.component';

describe('AddarretComponent', () => {
  let component: AddarretComponent;
  let fixture: ComponentFixture<AddarretComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddarretComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddarretComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusAppRoutingModule } from './bus-app-routing.module';
import {BusComponent} from './Bus/bus.component';
import {ArretComponent} from './Arret/arret.component';
import {VoyageurComponent} from './Voyageur/voyageur.component';
import {TrajetComponent} from './Trajet/trajet.component';

import {
  MatAutocompleteModule, MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDialogModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule,
  MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule, MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import {DataTableModule} from 'primeng/primeng';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import { NgxMatDatetimePickerModule, NgxMatTimepickerModule } from 'ngx-mat-datetime-picker';

import {AddComponent} from './Trajet/add/add.component';
import { AddbusComponent } from './Bus/addbus/addbus.component';
import { AddarretComponent } from './Arret/addarret/addarret.component';


@NgModule({
  declarations: [BusComponent, TrajetComponent, VoyageurComponent, ArretComponent,AddComponent, AddbusComponent, AddarretComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    DataTableModule,
    MatGridListModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    BusAppRoutingModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [AddComponent,AddbusComponent, AddarretComponent]
})
export class BusAppModule { }

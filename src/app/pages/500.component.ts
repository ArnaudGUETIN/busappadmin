import { Component } from '@angular/core';
import {Location} from "@angular/common";

import {Router} from "@angular/router";
import {LoginServiceService} from "../Services/login-service.service";

@Component({
  templateUrl: '500.component.html'
})
export class P500Component {

  constructor(private location:Location,
              private loginService:LoginServiceService,
              private router:Router) { }
  goBack(){
    if(!this.loginService.isTokenExpired() )
    {
      this.router.navigateByUrl('/pages/login');

    }
    else {
      this.location.back();
    }
  }
}

import {Component, OnInit} from '@angular/core';

import {Router} from "@angular/router";
import decode from 'jwt-decode';
import {NgxSpinnerService} from "ngx-spinner";
import {LoginServiceService} from "../Services/login-service.service";

@Component({
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit{
  badCredentials:Boolean=false;
  password="";
  username="";
ngOnInit(){

}
user={username:"",password:""};
  public token = localStorage.getItem('token');
  public authority:string;
  private redirect:string;
  constructor(private loginService:LoginServiceService,
              private route:Router,
              private spinner: NgxSpinnerService) { }

  onLogin(){
    this.spinner.show();
    this.user.username=this.username;
    this.user.password=this.password;
    if(this.user.username!="" && this.user.password!=""){

      this.loginService.attempAuthentification(this.user)
        .subscribe(data=>{
            let jwt=data.headers.get('Authorization');
         //   console.log(jwt);
            this.loginService.saveToken(jwt);

            const token = localStorage.getItem('token');
            const tokenPayload = decode(token);
         //   console.log(tokenPayload.sub);
            // this.loginService.updateUser(tokenPayload.sub)
            //   .subscribe(data=>{
            //     // console.log(data);
            //   });
            //
            // this.authority = tokenPayload.roles[0].authority;
            // if (this.authority == "GEntreprise") this.redirect = "/entreprise/entreprises";
            // else if (this.authority == "GRestaurant") this.redirect = "/restaurants/restaurant";
            // else if (this.authority == "Collaborateur") this.redirect = "/collaborateur/collaborateur";
            // else if (this.authority == "ADMIN") this.redirect = "/administration/admin";
            // this.route.navigateByUrl(this.redirect)
            setTimeout(() => {
              /** spinner ends after 5 seconds */
              this.spinner.hide();
              var newConnection:boolean=true;
              this.route.navigate(['/ecole/classes'],{ queryParams: { isNewConnection: newConnection } });
            }, 3000);

          },
          err=>{
            this.spinner.hide();
            this.badCredentials=true;
            this.route.navigate(['/ecole/classes'],{ queryParams: { isNewConnection: true } });
          })

    }
    else {
      this.spinner.hide();
      this.route.navigate(['/ecole/classes'],{ queryParams: { isNewConnection: true } });
      this.badCredentials=true;
    }

    }

}

import {Component, OnInit} from '@angular/core';
import {Location} from "@angular/common";

import {Router} from "@angular/router";
import {LoginServiceService} from "../Services/login-service.service";

@Component({
  templateUrl: '404.component.html'
})
export class P404Component implements OnInit{

  constructor(private  location:Location,
              private loginService:LoginServiceService,
              private router:Router) { }

   ngOnInit(){
   // console.log(this.loginService.isTokenExpired());
     if(this.loginService.isTokenExpired() )
     {
       this.router.navigateByUrl('/pages/login');

     }
   }

  goBack(){
   if(this.loginService.isTokenExpired() )
   {
     this.router.navigateByUrl('/pages/login');

   }
   else {
     this.location.back();
   }
  }
}

import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import {MatNativeDateModule,} from '@angular/material';
import {MatFormFieldModule,MatInputModule} from '@angular/material';
import {ToolbarModule} from 'primeng/toolbar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {SplitButtonModule} from 'primeng/primeng';
import {MatTableModule} from '@angular/material/table';
import { Module as StripeModule } from "stripe-angular";
import { AgmCoreModule } from '@agm/core';
@NgModule({
  imports: [
      StripeModule.forRoot(),
      DashboardRoutingModule,
    ChartsModule,
      SplitButtonModule,
      MatNativeDateModule,
      ToolbarModule,
      MatDatepickerModule,
      MatFormFieldModule,
      MatInputModule,
      MatTableModule,
      AgmCoreModule.forRoot({
          apiKey: 'AIzaSyAXyx2W92dGvNCD_6bbrHduonHuiBSXr-U'
      })
  ],
  declarations: [ DashboardComponent ]
})
export class DashboardModule { }



import { Component, OnInit } from '@angular/core';
import {StudentServiceService} from "../Services/student-service.service";
import {Router} from "@angular/router";
import {Student} from "../model/Student";
import {AnneeScolaireService} from "../Services/annee-scolaire.service";

@Component({
  selector: 'app-pameteres',
  templateUrl: './pameteres.component.html',
  styleUrls: ['./pameteres.component.scss']
})
export class PameteresComponent implements OnInit {
value:string="";
student:Student=new Student();
students:Student[]=[];
anneeScolaires:any[]=[];
currentAnneeScolaire:any={};
currentAnneeScolaireId:number=0;
  constructor(private studentService:StudentServiceService,
              private anneeScolaireService:AnneeScolaireService,
              private  router:Router) { }

  ngOnInit() {
    this.getAllAnneeScolaire();
  }

  getAllAnneeScolaire() {
    this.anneeScolaireService.getAllAnneeScolaires()
      .subscribe(data => {
        this.anneeScolaires = data;
        this.anneeScolaires.forEach(anneeScolaire=>{
          if(anneeScolaire.currentAnneeScolaire){
            this.currentAnneeScolaire=anneeScolaire;
            this.currentAnneeScolaireId=anneeScolaire.anneeScolaireId;
          }
        })
        console.log(this.currentAnneeScolaire);
      })
  }

  goTo(){
    this.studentService.getAllStudents()
      .subscribe(data=>{
        this.students=data;
        this.students.forEach(student=>{
          if(student.matricule==this.value){
            this.student=student;
            this.router.navigate(["ecole/student",student.studentId]);
          }
        })
      });



  }
  updateCurrentAnneeScolaire(){
    this.anneeScolaireService.changeCurrentAnneeScolaire(this.currentAnneeScolaireId)
      .subscribe(data=>{
        this.getAllAnneeScolaire();
      })
  }
}

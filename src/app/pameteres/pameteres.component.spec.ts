import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PameteresComponent } from './pameteres.component';

describe('PameteresComponent', () => {
  let component: PameteresComponent;
  let fixture: ComponentFixture<PameteresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PameteresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PameteresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

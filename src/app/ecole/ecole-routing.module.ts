import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EcoleComponent } from '../Ecole/ecole/ecole.component';
import { ClasseComponent } from '../Ecole/classe/classe.component';
import { ClasseViewComponent } from '../Ecole/classe-view/classe-view.component';
import {ProgrammeComponent} from "./programme/programme.component";
import {CoursComponent} from "./cours/cours.component";
import {DevoirComponent} from "./devoir/devoir.component";
import {ProfessorComponent} from "./professor/professor.component";
import {DevoirViewComponent} from "./devoir-view/devoir-view.component";
import {StudentComponent} from "../ecole/student/student.component";

const routes: Routes = [
  {
    path: 'ecole',
    data: {
      title: 'Ecole'
    },

    children: [
      {
        path: 'classes',
        component: ClasseComponent,
        data: {
          title: 'classes'
        }
      },
      {
        path: 'classeView/:id',
        component: ClasseViewComponent,
        data: {
          title: 'Classe'
        }
      },
      {
        path: 'details',
        component: EcoleComponent,
        data: {
          title: 'Details'
        }
      },
      {
        path: 'programmes',
        component: ProgrammeComponent,
        data: {
          title: 'Programmes'
        }
      },
      {
        path: 'cours',
        component: CoursComponent,
        data: {
          title: 'Cours'
        }
      },
      {
        path: 'devoirs',
        component: DevoirComponent,
        data: {
          title: 'Devoirs'
        }
      },
      {
        path: 'devoirView/:idDev/:idClasse',
        component: DevoirViewComponent,
        data: {
          title: 'Devoir'
        }
      },
      {
        path: 'professors',
        component: ProfessorComponent,
        data: {
          title: 'Professors'
        }
      },
      {
        path:'student/:idStudent',
        component:StudentComponent,
        data:{
          title:'Etudiant'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EcoleRoutingModule { }

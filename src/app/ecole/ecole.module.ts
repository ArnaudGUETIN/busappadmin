import {NgModule, Pipe} from '@angular/core';
import { CommonModule } from '@angular/common';

import { EcoleRoutingModule } from './ecole-routing.module';
import { EcoleComponent } from '../Ecole/ecole/ecole.component';
import { ClasseComponent } from '../Ecole/classe/classe.component';
import { ClasseViewComponent } from '../Ecole/classe-view/classe-view.component';
import {AccordionModule, CalendarModule, DataTableModule, DropdownModule, MultiSelectModule} from "primeng/primeng";
import {ButtonModule} from "primeng/button";
import {DataViewModule} from "primeng/dataview";
import {TableModule} from "primeng/table";
import {DialogModule} from "primeng/dialog";
import {FormsModule} from "@angular/forms";
import {FullCalendarModule} from "primeng/fullcalendar";
import { DevoirComponent } from './devoir/devoir.component';
import { ProgrammeComponent } from './programme/programme.component';
import { CoursComponent } from './cours/cours.component';
import { ContenteditableModule } from '@ng-stack/contenteditable';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import { ProfessorComponent } from './professor/professor.component';
import {FilterPipe} from "../Utils/CustomsPipes";
import {ToastModule} from "primeng/toast";
import {PanelModule} from 'primeng/panel';
import {PopoverModule} from "ngx-popover";
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatTabsModule} from '@angular/material/tabs';
import {DevoirViewComponent} from "./devoir-view/devoir-view.component";
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {
  MatCardModule,
  MatCheckboxModule, MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule
} from "@angular/material";

import {BrowserModule} from "@angular/platform-browser";
import {jqxSchedulerComponent} from "jqwidgets-scripts/jqwidgets-ts/angular_jqxscheduler";
import {jqxBarGaugeComponent} from "jqwidgets-scripts/jqwidgets-ts/angular_jqxbargauge";
import {jqxButtonComponent} from "jqwidgets-scripts/jqwidgets-ts/angular_jqxbuttons";
import { StudentComponent } from './student/student.component';
import {jqxDateTimeInputComponent} from "jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput";
import {jqxInputComponent} from "jqwidgets-scripts/jqwidgets-ts/angular_jqxinput";
import {jqxComboBoxComponent} from "jqwidgets-scripts/jqwidgets-ts/angular_jqxcombobox";
import {QRCodeModule} from "angularx-qrcode";
import {BarecodeScannerLivestreamModule} from "ngx-barcode-scanner";
import {DeviceDetectorModule} from "ngx-device-detector";
import {NgxBarcodeModule} from "ngx-barcode";





@NgModule({
  declarations: [jqxComboBoxComponent,jqxInputComponent,jqxDateTimeInputComponent,jqxSchedulerComponent,jqxButtonComponent,jqxBarGaugeComponent,FilterPipe,EcoleComponent, ClasseComponent, ClasseViewComponent, DevoirComponent, ProgrammeComponent, CoursComponent, ProfessorComponent,DevoirViewComponent, StudentComponent],
  imports: [
    CommonModule,
    QRCodeModule,
    BarecodeScannerLivestreamModule,
    // SchedulerModule,
    EcoleRoutingModule,
    DataTableModule,
    ButtonModule,
    TableModule,
    DataViewModule,
    DropdownModule,
    MultiSelectModule,
    DialogModule,
    FormsModule,
    AccordionModule,
    FullCalendarModule,
    CalendarModule,
    ContenteditableModule,
    ConfirmDialogModule,
    ToastModule,
    PanelModule,
    PopoverModule,
    DragDropModule,
    MatTabsModule,
    MatTableModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatCardModule,
    NgxBarcodeModule


  ]
})
export class EcoleModule { }

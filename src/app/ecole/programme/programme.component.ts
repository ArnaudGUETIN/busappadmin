import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-programme',
  templateUrl: './programme.component.html',
  styleUrls: ['./programme.component.scss']
})
export class ProgrammeComponent implements OnInit {

  constructor(private  router:Router) { }

  ngOnInit() {
  }
  getCours(){
    this.router.navigate(["ecole/cours"]);
  }
  getDevoirs(){
    this.router.navigate(["ecole/devoirs"]);
  }
}

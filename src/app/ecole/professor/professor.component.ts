import { Component, OnInit } from '@angular/core';
import {ProfessorService} from "../../Services/professor.service";
import {Professor} from "../../model/Professor";
import {ConfirmationService} from "primeng/api";

@Component({
  selector: 'app-professor',
  templateUrl: './professor.component.html',
  styleUrls: ['./professor.component.scss']
})
export class ProfessorComponent implements OnInit {
  professors:Professor[]=[];
  constructor(private professorService:ProfessorService,
              private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.getProfessors();
  }

  getProfessors() {
    this.professorService.getAllProfessorsOfScholl(1)
      .subscribe(data => {
        this.professors = data;
       // console.log(this.professors);
      })
  }

  addProfessor(){
    var professor:Professor = new Professor();
    professor.editable=true;
    this.professors = [...this.professors,professor];
  }
  modifyProfessor(professor:Professor){
    professor.editable=true;
  }

  cancelProfessorModif(professor:Professor){
    professor.editable=false;
  }

  saveMatiere(professor:Professor){
    if(professor.professorId!=0){
      this.professorService.updateProfessor(professor)
        .subscribe(data=>{
          // matiere=data;
          //this.addMatiereToProfessor(matiere);
          professor.editable=false;

        })
    }else {
      this.professorService.addProfessor(professor)
        .subscribe(data=>{
          professor.professorId=data["professorId"];
          this.addProfessorToSchool(professor);
        })
    }

  }

  addProfessorToSchool(professor:Professor){
    this.professorService.addProfessorToSchool(professor.professorId,1)
      .subscribe(data=>{
        professor.editable=false;
      })
  }

  confirm(professorId:number) {
    this.confirmationService.confirm({
      message: 'Etes-vous sur de vouloir supprimer cet element?',
      accept: () => {
        this.removeProfessor(professorId);
      }
    });
  }

  removeProfessor(professorId:number){
    if(professorId!=0){
      this.professorService.removeProfessor(professorId)
        .subscribe(data=>{
          this.getProfessors()
        })
    }else {
      this.professors.forEach(prof=>{
        if(prof.professorId==0){
          this.professors.splice(this.professors.indexOf(prof),1);
        }
      })
    }

  }
}

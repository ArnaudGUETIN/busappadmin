import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {StudentServiceService} from "../../Services/student-service.service";
import {Student} from "../../model/Student";
import {Matiere} from "../../model/Matiere";
import {MatiereService} from "../../Services/matiere.service";
import {Professor} from "../../model/Professor";
import {ProfessorService} from "../../Services/professor.service";
import {CoursService} from "../../Services/cours.service";
import {DatePipe} from "@angular/common";
import {ConfirmationService} from "primeng/api";
import {ClasseServiceService} from "../../Services/classe-service.service";
import {Classe} from "../../model/Classe";

@Component({
  selector: 'app-classe-view',
  templateUrl: './classe-view.component.html',
  styleUrls: ['./classe-view.component.scss'],
})
export class ClasseViewComponent implements OnInit {
  idClasse:number;
  displayProfSelect:boolean=false;
  editingMode:boolean=false;
  displayDialog:boolean=false;
  date:Date=new Date();
  displayEventDialog:boolean=false;
  selectedMatiereId:number=0;
  selectedMatiere:Matiere=new Matiere();
  professors:Professor[]=[];
  professorsList:Professor[]=[];
  matieres:Matiere[]=[];
  students:Student[]=[];
  student:Student=new Student;
  cols:any=[];
  pages:Array<number>=[];
  pageSize:number=5;
  currentPage:number=0;
  totalPages:number;
  events: any[]=[];
  coursList:any=[];
  programmes:any[];
  options: any;
  displayDialogForMatiere:boolean=false;
  matiereList:Matiere[]=[];
  classe:Classe=new Classe();

  constructor(private route:ActivatedRoute,
              private router:Router,
              private studentService:StudentServiceService,
              private coursService:CoursService,
              private matiereService:MatiereService,
              private professorService:ProfessorService,
              private datePipe:DatePipe,
              private confirmationService: ConfirmationService,
              private classeService:ClasseServiceService) { }

  ngOnInit() {
    this.idClasse=+this.route.snapshot.params['id'];

    this.classeService.getOneClasse(this.idClasse)
      .subscribe(data=>{
        this.classe=data;
      })
    this.cols = [
      { field: 'matricule', header: 'Matricule' },
      { field: 'name', header: 'Nom' },
      { field: 'surname', header: 'Prénom' },
      { field: 'email', header: 'Email' },
      { field: 'view', header: 'Ouvrir' }
    ];
   this.professorService.getAllProfessorsOfScholl(1)
     .subscribe(data=>{
       this.professorsList=data;
       this.getSubListPagination();
     })
    this.studentService.getAllStudentsOfClasse(this.idClasse)
      .subscribe((data:Student[])=>{
        this.students=data;
      })
    this.getMatieres();
    this.getMatiereList();
    this.coursService.getAllCoursOfClasse(this.idClasse)
      .subscribe(data=>{
        this.coursList=data;
       // console.log(this.coursList);
        this.coursList.forEach(cours=>{
          //  console.log(cours);
          this.events.push({
            "title": cours['libelle'],
            "start": this.datePipe.transform(cours['heureDebut'], 'yyyy-MM-dd HH:mm'),
            "end": this.datePipe.transform(cours['heureFin'], 'yyyy-MM-dd HH:mm'),
            "color": cours['couleur']
          });
        });
        this.programmes=this.events;
        //   console.log(this.events);
      })
    this.options = {
      defaultDate: new Date(),
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      dateClick : (e) =>  {
        //handle date click
       // console.log(e);
      },
      editable: true,
      eventLimit:true,
      eventResizableFromStart:true,

    };
  }

  getMatieres() {
    this.matiereService.getAllMatieresOfClasse(this.idClasse)
      .subscribe(data => {
        this.matieres = data;
      });
  }

  showDialogToAdd() {
    this.student=new Student;
    this.displayDialog = true;
  }
  showEventDialogToAdd() {
    this.date=new Date();
    this.displayEventDialog = true;
  }
showProfSelect(matiere:Matiere){
    this.selectedMatiere=matiere;
    this.displayProfSelect=true;
}

  affectProfessor(professor:Professor){

    this.matieres.forEach(mat=>{
      if(mat.matiereId == this.selectedMatiere.matiereId){
        mat.professorId=professor.professorId;
        mat.professorName=professor.name+" "+professor.surname;
      }
    })
    this.displayProfSelect=false;
  }
  save(){
    this.studentService.addStudent(this.student)
      .subscribe(data=>{
        this.student=data;
        this.studentService.addStudentToClasse(this.student.studentId,this.idClasse)
          .subscribe(data=>{
            //this.student=data;
//            console.log(this.student);
//            console.log(this.students);
            this.students = [...this.students, this.student];
            this.students.sort(function (item1,item2){
             return item1.name.localeCompare(item2.name);
            });
          })
      });
    this.displayDialog = false;
    this.ngOnInit();
  }
  choseMatiere(){
    this.displayDialogForMatiere=true;
  }
  addMatiere(){
    var matiere=this.matiereList.filter(obj => {
      return obj.matiereId == this.selectedMatiereId;
    });
    matiere[0].editable=true;
    this.matieres = [...this.matieres,matiere[0]];
    this.selectedMatiereId=0;
    this.displayDialogForMatiere=false;
  }

  cancelEdit(){
    this.editingMode=false;
  }
  hideDialog(){
    this.displayDialog = false;
    this.displayDialogForMatiere=false;
    this.displayProfSelect=false;
  }
  modifyMatiere(matiere:Matiere){
    matiere.editable=true;
  }

  cancelMatiereModif(matiere:Matiere){
    matiere.editable=false;
  }
  saveMatiere(matiere:Matiere){
    matiere.matiereId=0;
    if(matiere.matiereId!=0){
      this.matiereService.updateMatiere(matiere)
        .subscribe(data=>{
          // matiere=data;
          this.addMatiereToProfessor(matiere);

        })
    }else {
      this.matiereService.addMatiere(matiere)
        .subscribe(data=>{
          matiere.matiereId=data["matiereId"];
          this.addMatiereToProfessor(matiere);
          this.addMatiereToClasse(matiere);
        })
    }

  }

  addMatiereToProfessor(matiere: Matiere) {
    if(matiere.professorId!=0){
      this.matiereService.addMatiereToProfessor(matiere.matiereId, matiere.professorId)
        .subscribe(data => {
          matiere.editable = false;
          //console.log(data);
        })
    }
  }
  addMatiereToClasse(matiere: Matiere) {
    this.matiereService.addMatiereToClasse(matiere.matiereId, this.idClasse)
      .subscribe(data => {
        matiere.editable = false;
       // console.log(data);
      })
  }

  confirm(matiereId:number) {
    this.confirmationService.confirm({
      message: 'Etes-vous sur de vouloir supprimer cet element?',
      accept: () => {
        this.removeMatiere(matiereId);
      }
    });
  }

  removeMatiere(matiereId:number){
    if(matiereId!=0){
      this.matiereService.removeMatiere(matiereId)
        .subscribe(data=>{
          this.getMatieres();
        })
    }else {
      this.matieres.forEach(mat=>{
        if(mat.matiereId==0){
          this.matieres.splice(this.matieres.indexOf(mat),1);
        }
      })
    }

  }

  onRowSelect(event) {
    this.student = this.cloneStudent(event.data);
   // console.log(event.data);
    this.displayDialog = true;
  }

  cloneStudent(student: Student): Student {
    let s = new Student();
    for (let prop in student) {
      s[prop] = student[prop];
    }
    return s;
  }

  getMatiereList(){
    this.matiereService.getAllMatieres()
      .subscribe(data=>{
        this.matiereList=data;
      })
  }

  gotoPage(i:number){ this.currentPage=i; this.getSubListPagination(); }

  getSubListPagination(){
    this.professors=[];
    this.professors=[...this.professorsList];
   // console.log(this.professors[0]);
    let start=this.currentPage*this.pageSize;
    let end=this.pageSize*(this.currentPage+1);

    this.professors=this.professors.slice(start,end);

    this.totalPages= Math.floor(this.professorsList.length/this.pageSize)+1;

    // if(this.totalPages % this.pageSize !=0) this.totalPages+=1;
    this.pages=new Array(this.totalPages);
//    console.log(start+" "+end+" "+this.totalPages+" "+this.pages);
  }

  updateClasse(){
    this.classeService.updateClasse(this.classe)
      .subscribe(data=>{
       // this.classe=data;
      })
  }

  selectCarWithButton(student: Student) {
    this.router.navigate(["ecole/student",student.studentId]);
  }
}

import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {StudentServiceService} from "../../Services/student-service.service";
import {ActivatedRoute} from "@angular/router";
import {Adresse, Student} from "../../model/Student";
import {jqxInputComponent} from "jqwidgets-scripts/jqwidgets-ts/angular_jqxinput";
import {jqxDateTimeInputComponent} from "jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput";
import {jqxBarGaugeComponent} from "jqwidgets-scripts/jqwidgets-ts/angular_jqxbargauge";
import {Observable} from "rxjs/index";
import {UploadServiceService} from "../../Services/upload-service.service";
import {BarecodeScannerLivestreamComponent} from "ngx-barcode-scanner";

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit,AfterViewInit {
studentId:number=0;
student:Student=new Student();
moyennesList:any=[];
selectedIndex=0;
  editable:boolean=false;
  source: any[] = this.generateListePays();
  constructor(private studentService:StudentServiceService,
              private route: ActivatedRoute,
              private uploadService:UploadServiceService) { }

  @ViewChild(BarecodeScannerLivestreamComponent)
  barecodeScanner: BarecodeScannerLivestreamComponent;

  barcodeValue;
  @ViewChild('jqxDateTimeInputComponent') myInput: jqxDateTimeInputComponent;
  ngOnInit() {
    this.studentId=+this.route.snapshot.params['idStudent'];
    this.getStudent();
  }
  ngAfterViewInit(): void
  {
  //  this.barecodeScanner.start();
    this.getStudent();
  }

  onValueChanges(result){
    this.barcodeValue = result.codeResult.code;
    console.log(this.barcodeValue);
  }
  private getStudent() {
    this.studentService.getOneStudent(this.studentId)
      .subscribe(data => {
        this.student = data;
        if(this.student.adresse==null){
          this.student.adresse=new Adresse();
        }
        console.log(this.student);
        this.getNotes();
        this.getMoyennes(this.student.studentId);
        this.uploadService.getStudentImage(this.student.studentId,'profile-image1');
      })
  }
saveProfil(){
    setTimeout(()=>{
      this.uploadService.uploadStudentImage(this.student.studentId);
      setTimeout(()=>{
        this.uploadService.getStudentImage(this.student.studentId,'profile-image1');
      },2000)
    },2000)

}
  saveModification(){
    this.student.joinday=new Date(this.student.joinday);
    this.student.birthday=new Date(this.student.birthday);
  //  this.uploadService.uploadStudentImage(this.student.studentId);
   this.studentService.update(this.student)
     .subscribe(data=>{
       this.student=data;
       this.editable=false;
     })
  }
  // onSelect(event: any): void {
  //   let args = event.args;
  //   if (args != undefined) {
  //     let item = event.args.item;
  //     if (item != null) {
  //       this.myPanel.prepend(`<div style="margin-top: 5px;">Selected: ${item.label}</div>`);
  //     }
  //   }
  // }
  modify(){
    this.editable=true;
    this.student.birthday=new Date(this.student.birthday);
    this.student.joinday=new Date(this.student.joinday);
    this.student.nationnalite=this.student.nationnalite;
    this.selectedIndex=this.studentService.getCountries().indexOf(this.student.nationnalite);
  }
  cancelModification(){
      this.editable=false;
  }
  getMoyennes(studentId:number){
    this.studentService.getMoyennesOfStudent(studentId)
      .subscribe(data=>{
        this.moyennesList=data;
        console.log(this.moyennesList);
      })
  }
  generateListePays(): any[]{
    let countries:any=[];
    let source = [];
    countries=this.studentService.getCountries();
    this.selectedIndex=countries.indexOf(this.student.nationnalite);
    source=countries;


return source;
  }

  tooltip: any =
    {
      visible: true,
      formatFunction: (value: any, index: number) => {
        //let realVal = parseInt(value);
//        console.log(this.selectedElements);
        console.log(value+' '+index);
        return ('Note de ' + this.selectedElements.get(index));
      }
    }
  values: number[] = [12.36, 13.5, 15, 14.2,17.5,11];
  selectedElements:Map<number,string>=new Map();
  @ViewChild('myBarGauge') myBarGauge: jqxBarGaugeComponent;
  getNotes(){
    let arrayOfItems:number[]=[];
    let i=0;
    this.student['notes'].forEach(note=>{
     // console.log(note.value);
      arrayOfItems.push(note.value);
     // arrayOfItems.push(note.devoir.matiere.libelle);
      this.selectedElements.set(i,note.devoir.matiere.libelle);
      i++;
    })
   // if(arrayOfItems.length!=0){
   //   this.myBarGauge.values(arrayOfItems);
   // }
    this.myBarGauge.values(arrayOfItems);

    //console.log(arrayOfItems);
  }

  upload(){
    (<HTMLInputElement>document.getElementById('profile-image1')).src="../assets/img/avatars/man.png";
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevoirViewComponent } from './devoir-view.component';

describe('DevoirViewComponent', () => {
  let component: DevoirViewComponent;
  let fixture: ComponentFixture<DevoirViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevoirViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevoirViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

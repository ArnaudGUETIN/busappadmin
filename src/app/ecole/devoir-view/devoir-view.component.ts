import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, Sort} from "@angular/material";
import {ActivatedRoute} from "@angular/router";
import {StudentServiceService} from "../../Services/student-service.service";
import {Student} from "../../model/Student";
import {DevoirService} from "../../Services/devoir.service";
import {Devoir} from "../../model/Devoir";
import {Note} from "../../model/Note";
import {PeriodicElement} from "../../dashboard/dashboard.component";
import {SelectionModel} from "@angular/cdk/collections";
import {NoteService} from "../../Services/note.service";

@Component({
  selector: 'app-devoir-view',
  templateUrl: './devoir-view.component.html',
  styleUrls: ['./devoir-view.component.scss']
})
export class DevoirViewComponent implements OnInit {
  idClasse:number=0;
  idDevoir:number=0;
  displayedColumns: string[] = ['select','position','name', 'surname', 'email','note'];
  dataSource:any=new MatTableDataSource<Student>();
  devoir:Devoir=new Devoir();
  students:Student[]=[];
  editable:boolean=false;
  loading:boolean=true;
  selection:any=new SelectionModel<Student>(true, []);
  constructor(private route:ActivatedRoute,
              private studentService:StudentServiceService,
              private devoirService:DevoirService,
              private noteService:NoteService) { }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    this.idClasse=+this.route.snapshot.params['idClasse'];
    this.idDevoir=+this.route.snapshot.params['idDev'];
   // console.log(this.idDevoir+" "+this.idClasse);

    this.getStudends();

   // displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  //  dataSource = new MatTableDataSource(ELEMENT_DATA);
  }

  getStudends(){

    this.devoirService.getDevoir(this.idDevoir)
      .subscribe(data=>{
        this.devoir=data;
        console.log(this.devoir);
        let i=1;
        this.devoir.notes.forEach(note=>{
          let noteItem = new Note();
          noteItem.quotient=note.quotient;
          noteItem.value=note.value;
          noteItem.noteId=note.noteId;
          note['student'].note=noteItem;
          note['student']['position']=i++;
          this.students.push(note['student']);
        });
        setTimeout(()=>{
          this.loading=false;
        },2000);
        this.students.sort(function (item1,item2){
          return item1.name.localeCompare(item2.name);
        });
        this.dataSource = new MatTableDataSource<Student>(this.students);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.selection=new SelectionModel<Student>(true, []);
      });


  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Student): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row['position'] + 1}`;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  updateNotes(){
    let notes:Note[]=[];
    this.students.forEach(student=>{
      notes.push(student.note);
    });
   // console.log(notes);
    this.noteService.updateNotes(notes)
      .subscribe(data=>{
        notes =data;
      })
  }

  cancelModification(){
    this.editable=false;
  }

  saveModification(){
    this.updateNotes();
    this.editable=false;
  }

  modify(){
   this.editable=true;
  }

  sortData(sort: Sort) {
    const data = this.dataSource.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource = data;
      return;
    }

    this.dataSource = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'surname': return compare(a.surname, b.surname, isAsc);
        case 'email': return compare(a.email, b.email, isAsc);
        case 'note': return compare(a.note.value, b.note.value, isAsc);
        default: return 0;
      }
    });
    //this.dataSource = new MatTableDataSource<Student>(this.students);
  }
}
  function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }


import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ClasseServiceService} from "../../Services/classe-service.service";
import {CoursService} from "../../Services/cours.service";
import {DatePipe, Time} from "@angular/common";
import {Cours} from "../../model/Cours";
import {MatiereService} from "../../Services/matiere.service";
import {Matiere} from "../../model/Matiere";
import {jqxSchedulerComponent} from "jqwidgets-scripts/jqwidgets-ts/angular_jqxscheduler";

@Component({
  selector: 'app-cours',
  templateUrl: './cours.component.html',
  styleUrls: ['./cours.component.scss']
})
export class CoursComponent implements OnInit,AfterViewInit  {
  values: number[] = [102, 115, 130, 137];
  classeId:number=1;
  selectedClasse:number=0;
  matieres:Matiere[]=[];
  selectedMatiere:Matiere=new Matiere();
  classes:any=[];
  coursList:Cours[]=[];
  cours:Cours=new Cours();
  events: any[]=[];
  options: any;
  source: any ={};
  dataAdapter: any ;
  date: any;
//  appointmentDataFields: any ;
  renderAppointment:any;
  resources:any;
  appointmentDataFields:any;
  constructor(private classeService:ClasseServiceService,
              private coursService:CoursService,
              private matiereService:MatiereService,
              private datePipe:DatePipe) { }

  ngOnInit() {
    this.classeService.getAllClassesOfSchool(1)
      .subscribe(data=>{
        this.classes=data;
      //  console.log(this.classes);
      });

    this.getCours(this.selectedClasse);





  }

  getCours(idClasse:number) {
    this.getMatieres();
    if(idClasse!=0){
      this.coursService.getAllCoursOfClasse(idClasse)
        .subscribe(data => {
          this.coursList = data;
           console.log(this.coursList);
          this.getSources();
        })
    }

  }




  getMatieres() {
    this.matiereService.getAllMatieresOfClasse(this.selectedClasse)
      .subscribe(data => {
        this.matieres = data;
      })
  }

  updateClasseProgramme(idClasse:number){
   // console.log(this.selectedClasse);
    this.events=[];
    this.selectedClasse=idClasse;
    console.log(idClasse);
    this.classeId=this.selectedClasse;
    this.getCours(idClasse);
    this.ngOnInit();

  }

















  @ViewChild('schedulerReference') scheduler: jqxSchedulerComponent;
  ngAfterViewInit(): void {
    if(this.selectedClasse!=0){
      this.scheduler.ensureAppointmentVisible('id1');
    }
  }
  getWidth() : any {
    if (document.body.offsetWidth < 850) {
      return '50%';
    }

    return 850;
  }

  generateAppointments(): any {
    let appointments = new Array();
    if(this.selectedClasse!=0){

      this.coursList.forEach(cours=>{
        cours.heureDebut = new Date(cours.heureDebut);
        cours.heureFin = new Date(cours.heureFin);
        let appointment ={
          id: cours.coursId,
          description: cours.libelle,
          location: "",
          subject: cours.libelle,
          calendar: cours.libelle,
          start: cours.heureDebut,// new Date(cours.heureDebut.getFullYear(0), cours.heureDebut.getMonth(0), cours.heureDebut.getDay(0), cours.heureDebut.getHours(0), 0, 0),
          end: cours.heureFin//new Date(cours.heureFin.getFullYear(0), cours.heureFin.getMonth(0), cours.heureFin.getDay(0), cours.heureFin.getHours(0), 0, 0)
        }
        appointments.push(appointment)
      })
      if(this.matieres!=null){
        this.matieres.forEach(matiere=>{
          let appointment ={

            subject: matiere.libelle,
            calendar: matiere.libelle,
            start:  new Date(1970,0,0),
            end:  new Date(1970,0,0)
          }
          appointments.push(appointment)
        })
      }

    }

    console.log(appointments);
    return appointments;
  };
  getSources(){


   this.source=
    {
      dataType: "json",
      dataFields: [
        { name: 'id', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'location', type: 'string' },
        { name: 'subject', type: 'string' },
        { name: 'calendar', type: 'string' },
        { name: 'start', type: 'date' },
        { name: 'end', type: 'date' }
      ],
      id: 'id',
      localData: this.generateAppointments()
    };
    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.date = new jqx.date(new Date());

  this.resources=
    {
      colorScheme: "scheme05",
      dataField: "calendar",
      source: new jqx.dataAdapter(this.source)
    };

    this.appointmentDataFields =
      {
        from: "start",
        to: "end",
        id: "id",
        description: "description",
        location: "location",
        subject: "subject",
        resourceId: "calendar"
      };

    this.renderAppointment = (data) => {
      // data Object properties
      // appointment - Object with the properties from the Scheduler.s source object.
      // width - int
      // height- int
      // textColor - hex
      // background - hex
      // borderColor - hex
      // style - string
      // cssClass - string
      // html - html string
      // view - string
      let img = '<img style="top: 2px; position: relative;" src="/assets/img/ticket.png"/>';

      if (data.view == 'weekView' || data.view == 'dayView' || data.view == 'monthView') {
        data.html = '<i class="icon-clock"> ' + data.appointment.subject + '</i>';
      }
      return data;
    };
  }
  views: any[] =
    [
      'dayView',
      'weekView',
      'monthView'
    ];


  mySchedulerOnAppointmentAdd(event: any): void {
    let appointment = event.args.appointment;
    this.cours.coursId=0;
    this.cours.libelle=appointment.subject;
    this.cours.heureDebut=appointment.originalData.start;
    this.cours.heureFin=appointment.originalData.end;
    let matiereLibelle=appointment.originalData.calendar;
    this.coursService.addCours(this.cours)
      .subscribe(data=>{
        this.cours=data;


        if(this.cours.coursId!=0){
          this.coursService.addCoursToClasse(this.cours.coursId,this.selectedClasse)
            .subscribe(data=>{

            })
          this.getMatieres();
          let matiereId=this.matieres.find(x => x.libelle == matiereLibelle).matiereId;
          this.coursService.addCoursToMatiere(this.cours.coursId,matiereId)
            .subscribe(data=>{
           //  this.getCours(this.selectedClasse);
            })
        }
      })
    console.log(this.cours);
  };

  mySchedulerOnAppointmentDelete(event: any): void {
    let appointment = event.args.appointment;
    console.log(appointment);
  };
  mySchedulerOnAppointmentChange(event: any): void {
    let appointment = event.args.appointment;
    this.cours.coursId=appointment.id;
    this.cours.libelle=appointment.subject;
    this.cours.heureDebut=appointment.originalData.start;
    this.cours.heureFin=appointment.originalData.end;
    this.coursService.updateCours(this.cours)
      .subscribe(data=>{

      })
    console.log(appointment);
  };

  exportSettings: any = {
    fileName: null
  };
  printClick(): void {
    let content = this.scheduler.exportData('html');
    let newWindow = window.open('', '', 'width=800, height=500'),
      document = newWindow.document.open(),
      pageContent =
        '<!DOCTYPE html>' +
        '<html>' +
        '<head>' +
        '<meta charset="utf-8" />' +
        '<title>jqxScheduler Print Preview</title>' +
        '</head>' +
        '<body>' + content + '</body></html>';
    try {
      document.write(pageContent);
      document.close();
      newWindow.print();
    }
    catch (er) {
      document.close();
    }
  };

}

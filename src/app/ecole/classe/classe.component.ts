import { Component, OnInit } from '@angular/core';
import {ClasseServiceService} from "../../Services/classe-service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Classe} from "../../model/Classe";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-classe',
  templateUrl: './classe.component.html',
  styleUrls: ['./classe.component.scss'],
  providers: [MessageService]
})
export class ClasseComponent implements OnInit {
classes:Classe[]=[];
classe:Classe = new Classe();
  motCle:string="";
  newConnection:boolean=true;
  displayDialog:boolean=false;
  constructor(private classeService:ClasseServiceService,
              private  router:Router,
              private route: ActivatedRoute,
              private messageService: MessageService) { }

  ngOnInit() {
    this.newConnection=this.route.snapshot.queryParams['isNewConnection'];

    if(this.newConnection){
     // console.log(this.newConnection);
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.helloMessage();
      }, 1000);

    }
    this.getClasses();

    //console.log(this.classes);
  }
  helloMessage() {
    this.messageService.add({severity:'success', summary: 'Success Message', detail:'Bienvenue dans votre espace'});
  }
  getClasses() {
    this.classeService.getAllClassesOfSchool(1)
      .subscribe(data => {
        this.classes = data;
      //  console.log(this.classes);
        var i=1;
        this.classes.forEach(classe=>{
         // console.log(i);
         // console.log(i%3);
        //  console.log(i%2);
          if(i%3==0){
            classe.couleur="bg-warning";
          }else if(i%2==0){
            classe.couleur="bg-success";
          }else {
            classe.couleur="bg-info";
          }
          i++;
        })
     //   console.log(this.classes);
      })
  }

  getClasse(classId:number){
    this.router.navigate(["ecole/classeView",classId]);
  }
  newClasse(){
    this.classe= new Classe();
    this.displayDialog = true;
  }
  hideDialog(){
    this.displayDialog = false;
  }

  saveClasse(){
    this.classeService.addClasse(this.classe)
      .subscribe(data=>{
        this.classe = data;

        this.classeService.addClasseToSchool(this.classe.classeId,1)
          .subscribe(data=>{
            this.displayDialog=false;
            this.classes = [...this.classes,this.classe];
            this.classe= new Classe();
          })
      })
  }
}

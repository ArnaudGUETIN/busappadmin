import { Component, OnInit } from '@angular/core';
import {MatiereService} from "../../Services/matiere.service";
import {Matiere} from "../../model/Matiere";
import {DevoirService} from "../../Services/devoir.service";
import {Devoir} from "../../model/Devoir";
import {MatTabChangeEvent} from "@angular/material";
import {ClasseServiceService} from "../../Services/classe-service.service";
import {Classe} from "../../model/Classe";
import {Router} from "@angular/router";

@Component({
  selector: 'app-devoir',
  templateUrl: './devoir.component.html',
  styleUrls: ['./devoir.component.scss']
})
export class DevoirComponent implements OnInit {
  matieres:Matiere[]=[];
  devoirs:Devoir[]=[];
  classes:Classe[]=[];
  selectedClasseId:number=0;
  newDevoir:Devoir=new Devoir();
  selectedMatiereId:number=0;
  displayDialog:boolean=false;
  constructor(private matiereService:MatiereService,
              private devoirService:DevoirService,
              private classeService:ClasseServiceService,
              private  router:Router) { }

  ngOnInit() {
    this.getClasses();




  }

  getMatieres(classeId:number){
    this.matiereService.getAllMatieresOfClasse(classeId)
      .subscribe(data=>{
        this.matieres=data;
        if(this.matieres.length!=0) {
        //  console.log(this.selectedMatiereId);
          this.selectedMatiereId = this.matieres[0].matiereId;
        //  console.log(this.selectedMatiereId);
          this.getDevoirs(this.selectedMatiereId);
        }
      })
  }

  getDevoirs(idMatiere:number){
    this.devoirService.getAllDevoirsOfMatiere(idMatiere)
      .subscribe(data=>{
        this.devoirs=data;
       // console.log(this.devoirs);
      })
  }
  showDevoir(matiereId:number){
    this.displayDialog=true;
    this.selectedMatiereId=matiereId;
  }

  hideDialog(){
    this.displayDialog=false;
  }

  saveDevoir(){
    this.devoirService.addDevoir(this.newDevoir)
      .subscribe(data=>{
        this.newDevoir=data;

        this.devoirService.addDevoirToMatiere(this.newDevoir.devoirId,this.selectedMatiereId,this.selectedClasseId)
          .subscribe(data=>{
            this.devoirs =[...this.devoirs,this.newDevoir];
          })
      })
    this.displayDialog=false;
    this.newDevoir=new Devoir();
  }

  changeIndex(event: MatTabChangeEvent){
    // console.log('event => ', event);
    // console.log('index => ', event.index);
    // console.log('tab => ', event.tab.textLabel);

    this.matieres.forEach(mat=>{
      if(mat.libelle==event.tab.textLabel){
        this.selectedMatiereId=mat.matiereId;
      }
    })
    this.getDevoirs(this.selectedMatiereId);
  }

  getClasses() {
    this.classeService.getAllClassesOfSchool(1)
      .subscribe(data => {
        this.classes = data;
        //  console.log(this.classes);
        var i=1;
        this.selectedClasseId = this.classes[0].classeId;
        //console.log(this.selectedClasseId);
        this.getMatieres(this.selectedClasseId);
        this.classes.forEach(classe=>{
          // console.log(i);
          // console.log(i%3);
          //  console.log(i%2);
          if(i%3==0){
            classe.couleur="bg-warning";
          }else if(i%2==0){
            classe.couleur="bg-success";
          }else {
            classe.couleur="bg-info";
          }
          i++;
        })
        //   console.log(this.classes);
      })
  }

  onClasseChange(classeId:number){
    this.selectedClasseId=classeId;
    //console.log(this.selectedClasseId);
    this.getMatieres(this.selectedClasseId);
  }

  getDevoir(devoirId:number){
    this.router.navigate(["ecole/devoirView",devoirId,this.selectedClasseId]);
  }
}

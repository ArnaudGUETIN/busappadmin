import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Location} from "@angular/common";
import {SharedService} from "../Services/shared.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {
  private  items;
  private loggedIn: boolean=false;
  public username=localStorage.getItem('username');
  public token = localStorage.getItem('token');
  public authority:string;
  public title:string="";
  public disabled = false;
  public status: {isopen: boolean} = {isopen: false};
  metrics:any={};
  constructor(private router:Router,
              private location: Location,
              private sharedService:SharedService){

  }
  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  ngOnInit(): void {
    // this.sharedService.getMetrics(1)
    //   .subscribe(data=>{
    //     this.metrics=data;
    //     console.log(this.metrics);
    //   })
  }

  onLogout(){
    localStorage.removeItem('token');
    this.loggedIn=false;
    this.router.navigateByUrl('/pages/login');
  }
  goBack(){
    this.location.back();
  }
}

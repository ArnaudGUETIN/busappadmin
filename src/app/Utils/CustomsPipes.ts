import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {
  transform(items: any[], filterBy: string): any {
    filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
    return filterBy ? items.filter((item) =>
      item.libelle.toLocaleLowerCase().indexOf(filterBy) !== -1) : items;
  }
}

import {Note} from "./Note";

export class Devoir{
  static FIRST_DEVOIR:number=1;
  static SECOND_DEVOIR:number=2;
  static THIRD_DEVOIR:number=3;

  devoirId:number;
  libelle:string;
  matiereName:string;
  classeName:string;
  date:Date;
  devoirOrder:number;
  notes:Note[];
  constructor(){
    this.devoirId=0;
    this.libelle="";
    this.matiereName="";
    this.classeName="";
    this.date = new Date();
    this.devoirOrder=0;
    this.notes=[];
  }
}

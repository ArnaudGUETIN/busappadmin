export class Classe{
  classeId:number;
  libelle:string;
  couleur:string;
  effectif:number;
  schoolName:string;
  commentaire:string;
  description:string;
  constructor(){
    this.classeId=0;
    this.libelle="";
    this.couleur="";
    this.effectif=0;
    this.schoolName="";
    this.commentaire="";
    this.description="";
  }
}

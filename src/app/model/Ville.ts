import {Arret} from "./Arret";
export class Ville {
  villeId:number;
  nom:string;
  arrets:Arret[]=[];

  constructor(){
    this.villeId=0;
    this.nom="";
    this.arrets=[];
  }
}

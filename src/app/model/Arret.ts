export class Arret {

  arretId:number;
  nom:string;
  longitude:number;
  latitude:number;
  nomVille:string;
  constructor(){
    this.arretId=0;
    this.nom="";
    this.longitude=0;
    this.latitude=0;
    this.nomVille="";
  }
}

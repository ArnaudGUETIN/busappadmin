import {Time} from '@angular/common';
import {Bus} from './Bus';
import {Arret} from './Arret';

export class Trajet {
  trajetId:number;
  dateDepart:Date;
  dateArrivee:Date;
  heureDepart:Time;
  heureArrivee:Time;
  bus:Bus;
  arretDepart:Arret;
  arretArrivee:Arret;
  duree:number;
  constructor(){
    this.trajetId=0;
    this.dateDepart= new Date();
    this.dateArrivee=new Date();
    this.bus = new Bus;
    this.arretDepart = new Arret();
    this.arretArrivee = new Arret();
    this.duree = 0;
  }
}

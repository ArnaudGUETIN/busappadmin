export class Cours{
  coursId:number;
  libelle:string;
  heureDebut:Date;
  heureFin:Date;
  couleur:string;
  constructor(){
    this.coursId=0;
    this.libelle="";
    this.couleur="";
    this.heureFin=new Date();
    this.heureDebut=new Date();
  }
}

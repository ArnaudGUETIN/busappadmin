
import {Note} from "./Note";

export class Student{

    studentId:number;
    name: string;
    surname: string;
    email: string;
    moyenne: number;
    matricule:string;
    nationnalite:string;
    birthday:Date;
    joinday:Date;
    placeOfBirth:string;
    rang :number;
    note:Note;
    adresse: Adresse;

  constructor(){
      this.studentId=0;
      this.name="";
      this.surname="";
      this.email="";
      this.matricule="";
      this.moyenne=0;
      this.rang=0;
      this.nationnalite="";
      this.birthday=new Date();
      this.joinday=new Date();
      this.placeOfBirth="";
      this.note=new Note();
      this.adresse=new Adresse();
  }


}

export class Adresse{
  country:string;
  ville:string;
  telephone:string;
  constructor(){
    this.country="";
    this.ville="";
    this.telephone="";
  }
}

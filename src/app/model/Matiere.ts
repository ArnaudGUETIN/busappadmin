export class Matiere{
  matiereId:number;
  libelle:string;
  couleur:string;
  professorName:string;
  professorId:number;
  editable:boolean;
  coeficient:number;
  constructor(){
    this.matiereId=0;
    this.libelle="";
    this.couleur="";
    this.professorName="";
    this.coeficient=1;
    this.professorId=0;
    this.editable=false;
  }
}

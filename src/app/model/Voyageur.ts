export class Voyageur {
  voyageurId:number;
  nom:string;
  prenom:string;
  email:string;
  dateNaissance:Date;
  numeroTelephone:string;
  constructor(){
    this.voyageurId = 0;
    this.nom="";
    this.prenom="";
    this.email="";
    this.dateNaissance = new Date();
    this.numeroTelephone="";
  }
}

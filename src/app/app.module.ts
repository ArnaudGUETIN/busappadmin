import {ErrorHandler, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {LocationStrategy, HashLocationStrategy, DatePipe} from '@angular/common';

import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';
import {SplitButtonModule} from 'primeng/splitbutton';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCardModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatListModule,
  MatSelectModule, MatTreeModule
} from '@angular/material';
import {ConfirmationService, MessageService} from 'primeng/api';

// Routing Module
import { AppRoutingModule } from './app.routing';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import {HttpClientModule} from "@angular/common/http";
import { Pipe, PipeTransform } from '@angular/core';
import {LoginServiceService} from "./Services/login-service.service";
import {SimpleLayoutComponent} from "./layouts/simple-layout.component";
import {AuthErrorHandler} from "./Services/AuthErrorHandler";
import {AuthGuard} from "./Services/AuthGuard";
import {jqxBarGaugeComponent} from "jqwidgets-scripts/jqwidgets-ts/angular_jqxbargauge";
import {UploadServiceService} from "./Services/upload-service.service";
import {PameteresComponent} from "./pameteres/pameteres.component";
import {FormsModule} from "@angular/forms";
import {TrajetService} from './Services/trajet.service';
import {ArretService} from './Services/arret.service';
import {BusService} from './Services/bus.service';





@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    FormsModule,
    ChartsModule,
    MatButtonModule,
    MatCheckboxModule,
    SplitButtonModule,
    HttpClientModule,
    MatListModule,
    MatCardModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatTreeModule

  ],
  exports: [MatButtonModule, MatCheckboxModule],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    SimpleLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    PameteresComponent,
    

  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  },{
     provide: ErrorHandler,
     useClass: AuthErrorHandler
   },
  //   {
  //     provide: RequestOptions,
  //     useClass: AuthRequestOptions
  //   },
    DatePipe,ConfirmationService,MessageService,AuthGuard,LoginServiceService,UploadServiceService,TrajetService,ArretService,BusService],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

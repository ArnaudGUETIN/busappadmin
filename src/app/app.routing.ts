import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import {SimpleLayoutComponent} from "./layouts/simple-layout.component";
import {AuthGuard} from "./Services/AuthGuard";
import {PameteresComponent} from "./pameteres/pameteres.component";

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'pages/404',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule',

      },
        {
            path: '',
            loadChildren: './professor/professor.module#ProfessorModule',
            data: {
                title: 'Professor'
            },
          canActivate: [AuthGuard],
        },
      {
        path: 'parametres',
        component: PameteresComponent,
        data: {
          title: 'Parametres'
        },
      },
        {
          path: '',
          loadChildren: './ecole/ecole.module#EcoleModule',
          data: {
            title: 'Ecole'
          },
          canActivate: [AuthGuard],
        },
      {
        path: '',
        loadChildren: './bus-app/bus-app.module#BusAppModule',
        data: {
          title: 'busapp'
        },
        // canActivate: [AuthGuard],
      },
    ]
  },

  {
    path: 'pages',
    component: SimpleLayoutComponent,
    data: {
      title: 'Pages'
    },
    children: [
      {
        path: '',
        loadChildren: './pages/pages.module#PagesModule',
      }
    ]
  },

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

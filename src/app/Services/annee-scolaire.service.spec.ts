import { TestBed } from '@angular/core/testing';

import { AnneeScolaireService } from './annee-scolaire.service';

describe('AnneeScolaireService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnneeScolaireService = TestBed.get(AnneeScolaireService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
const endpoint = environment.endpoint;
@Injectable({
  providedIn: 'root'
})
export class VoyageurService {
  constructor(public http:HttpClient) { }
  getAllVoyageurs():any{
    return this.http.get(endpoint+"allVoyageurs");
  }

  getOneVoyageur(idVoyageur:number){
    return this.http.get(endpoint+"voyageur/"+idVoyageur);
  }

  // createVoyageur(idVille:number,arret:any){
  //   return this.http.post(endpoint+"addArret?villeId="+idVille,arret);
  // }
}

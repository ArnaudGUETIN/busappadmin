import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
const endpoint = "http://localhost:8080/matieres/";
@Injectable({
  providedIn: 'root'
})
export class MatiereService {
  jwtToken:string;
  constructor(private http:HttpClient) {
    if(this.jwtToken==null) this.loadToken();
  }
  loadToken(){
    this.jwtToken=localStorage.getItem('token');
  }
  getMatiere(idMatiere:number){
    return this.http.get(endpoint+idMatiere,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
  getAllMatieres():any{
    return this.http.get(endpoint+"all",{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
  getAllMatieresOfClasse(idClasse:number):any{
    return this.http.get(endpoint+"classe/"+idClasse+"/matieres/all",{headers: new HttpHeaders({"Authorization":this.jwtToken})})
  }

  addMatiere(matiere:any):any{
    return this.http.post(endpoint+"add",matiere,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  updateMatiere(matiere:any):any{
    return this.http.put(endpoint+"update",matiere,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  addMatiereToProfessor(idMatiere:number,idProfessor:number){
    return this.http.get(endpoint+"professor/"+idProfessor+"/addTo/matiere/"+idMatiere,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  addMatiereToClasse(idMatiere:number,idClasse:number){
    return this.http.get(endpoint+"classe/"+idClasse+"/addTo/matiere/"+idMatiere,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  removeMatiere(matiereId:number){
    return this.http.delete(endpoint+"remove/"+matiereId,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
}

import { Injectable } from '@angular/core';
import {Note} from "../model/Note";
import {HttpClient, HttpHeaders} from "@angular/common/http";
const endpoint = "http://localhost:8080/notes/";
@Injectable({
  providedIn: 'root'
})
export class NoteService {

  jwtToken:string;
  constructor(private http:HttpClient) {
    if(this.jwtToken==null) this.loadToken();
  }
  loadToken(){
    this.jwtToken=localStorage.getItem('token');
  }
  getNote(idNote:number):any{
    return this.http.get(endpoint+idNote,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
  updateNotes(notes:Note[]):any{
    return this.http.put(endpoint+"update/all",notes,{headers: new HttpHeaders({"Authorization":this.jwtToken})})
  }
}

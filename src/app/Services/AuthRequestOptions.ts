import { Headers, Http, BaseRequestOptions } from '@angular/http';
import {HttpHeaders} from "@angular/common/http";


const AUTH_HEADER_KEY = 'Authorization';
const AUTH_PREFIX = 'Bearer';
const TOKEN_NAME = 'token';
export class AuthRequestOptions extends BaseRequestOptions {

  constructor() {
    super();
   let header=new HttpHeaders();
    const token = localStorage.getItem(TOKEN_NAME);
    if(token) {
      this.headers.append(AUTH_HEADER_KEY, `${token}`);
    }
  }

}

import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthErrorHandler implements ErrorHandler {

  constructor(private injector: Injector) { }

  handleError(error:any) {
    const router = this.injector.get(Router);
    //console.log(error);
    if (error.status === 401 || error.status === 403) {
      router.navigate(['/pages/login']);
    }else if (error.status === 404) {
      router.navigate(['/pages/404']);
    }

   /* if (error.rejection.status === 404 || error.rejection.status === 403) {
      router.navigate(['/pages/404']);
    }*/

    throw error;
  }
}

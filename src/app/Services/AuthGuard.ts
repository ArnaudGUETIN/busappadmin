import { Injectable } from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, ActivatedRoute} from '@angular/router';

import decode from 'jwt-decode';
import {LoginServiceService} from "./login-service.service";



@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private loginService: LoginServiceService,
    private route: ActivatedRoute) {}

  canActivate(route: ActivatedRouteSnapshot) {
    // const expectedRole = route.firstChild.data.expectedRole;
    // const token = localStorage.getItem('token');
    // let currentRoute = this.route.root
    //
    // if(token!=null){
    //   const tokenPayload = decode(token);
    //
    // }
    //
    //
    // if (!this.loginService.isTokenExpired()) {
    //   return true;
    // }
    //
    // this.router.navigate(['pages/login']);
    return true;
  }

}

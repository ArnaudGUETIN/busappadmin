import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import * as jwt_decode from 'jwt-decode';
import decode from 'jwt-decode';
import {environment} from "../../environments/environment"
@Injectable()
export class LoginServiceService {
  jwtToken:string;
  constructor(private http: HttpClient) { }

    attempAuthentification(user){
    return this.http.post(environment.apiEndpointS+"login",user,{observe:'response'});
  }

  saveToken(jwt:string){
    const tokenPayload = decode(jwt);
    localStorage.setItem('username',tokenPayload.sub);
    localStorage.setItem('token',jwt);
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) return null;

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  public isTokenExpired(token?: string): boolean {
    if(!token) token = this.loadToken();
    if(!token) return true;

    const date = this.getTokenExpirationDate(token);
    if(date === undefined) return false;
    return !(date.valueOf() > new Date().valueOf());
  }
  loadToken(){
    return localStorage.getItem('token');
  }
  updateUser(username:string){
    if (this.jwtToken == null) this.loadToken();
    return this.http.put(environment.apiEndpointS+"authentification/user/update",username,)
  }

}

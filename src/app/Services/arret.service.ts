import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
const endpoint = environment.endpoint;
@Injectable({
  providedIn: 'root'
})
export class ArretService {
  constructor(public http:HttpClient) { }
  getAllArret():any{
    return this.http.get(endpoint+"allArrets");
  }

  getOneArret(idArret:number){
    return this.http.get(endpoint+"arret/"+idArret);
  }

  createArret(idVille:number,arret:any){
    return this.http.post(endpoint+"addArret?villeId="+idVille,arret);
  }
}

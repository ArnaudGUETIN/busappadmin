import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Trajet} from '../model/Trajet';
const endpoint = environment.endpoint;
@Injectable({
  providedIn: 'root'
})
export class TrajetService {

  constructor(public http:HttpClient) { }

  getAlltrajets():any{
    return this.http.get(endpoint+"allTrajets");
  }

  getOneTrajet(idTrajet:number){
    return this.http.get(endpoint+"trajet/"+idTrajet);
  }


  searchArajets(idArretDept:number,idArretArr:number,date:any){
    return this.http.get(endpoint+"search?idArretDepart="+idArretDept+"&idArretArrivee="+idArretArr+"&date="+date);
  }

  createTrajet(arretDeptId:number,arretArrId:number,busId:number,trajet:Trajet){
    return this.http.post(endpoint+"addTrajet?arretDeptId="+arretDeptId+"&arretArrId="+arretArrId+"&busId="+busId,trajet);
  }
}

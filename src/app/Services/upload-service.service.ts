import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment"
const endpoint = "http://localhost:8080/students/";
@Injectable()
export class UploadServiceService {

  filesToUpload: Array<File>;

  constructor() {
    this.filesToUpload = [];
  }

  uploadArticleImage(idArt: number) {
    this.makeFileRequest(environment.apiEndpointR+"articles/add/image?id="+idArt, [], this.filesToUpload).then((result) => {
      console.log(result);
    }, (error) => {
      console.log(error);
    });
  }
  uploadStudentImage(idC: number) {
    this.makeFileRequest(endpoint+"add/image?id="+idC, [], this.filesToUpload).then((result) => {
      console.log(result);
    }, (error) => {
      console.log(error);
    });
  }

  modifyStudentImage(idC: number) {
    console.log(this.filesToUpload);
    if (this.filesToUpload.length>0) {
      this.makeFileRequest(endpoint+"update/image?id="+idC, [], this.filesToUpload).then((result) => {
        console.log(result);
      }, (error) => {
        console.log(error);
      });
    }
  }

  modifyArticleImage(idArt: number) {
    console.log(this.filesToUpload);
    if (this.filesToUpload.length>0) {
      this.makeFileRequest(environment.apiEndpointR+"articles/update/image?id="+idArt, [], this.filesToUpload).then((result) => {
        console.log(result);
      }, (error) => {
        console.log(error);
      });
    }
  }

  fileChangeEvent(fileInput: any,htmlId:string,id:number) {
    this.filesToUpload = <Array<File>> fileInput.target.files;
    console.log(fileInput.target.files);
   // (<HTMLInputElement>document.getElementById(htmlId)).src="../assets/img/avatars/man.png";
    this.uploadStudentImage(id);
    //setTimeout()
    console.log(htmlId);
    setTimeout(()=>{
      this.getStudentImage(id,htmlId);
    },1000)

  }

  makeFileRequest(url:string, params:Array<string>, files:Array<File>) {
    return new Promise((resolve, reject) => {
      var formData:any = new FormData();
      var xhr = new XMLHttpRequest();
      for(var i=0; i<files.length;i++) {
        formData.append("uploads[]", files[i], files[i].name);
      }
      xhr.onreadystatechange = function() {
        if(xhr.readyState == 4) {
          if(xhr.status==200) {
            console.log("image uploaded successfully!");
          } else {
            reject(xhr.response);
          }
        }
      }

      xhr.open("POST", url, true);
      xhr.setRequestHeader("Authorization", localStorage.getItem("token"));
      xhr.send(formData);
    });
  }

  getStudentImage(id:number,htmlid:string){

    var xhr = new XMLHttpRequest();
    xhr.open("GET", endpoint+"image/"+id , true);
    xhr.setRequestHeader("Authorization", localStorage.getItem("token"));
    xhr.responseType = "arraybuffer";
    xhr.onload = function (oEvent) {
      var arrayBuffer = xhr.response; // Note: not oReq.responseText
      if (arrayBuffer) {

        var u8 = new Uint8Array(arrayBuffer);
        var b64encoded = btoa(String.fromCharCode.apply(null, u8));
        console.log();
        if (u8.length){
          var mimetype = "image/png"; // or whatever your image mime type is childNodes[1].getAttribute('src')
          console.log('ici');
          (<HTMLInputElement>document.getElementById(htmlid)).src ="data:" + mimetype + ";base64," + b64encoded;
        }
        else {
          (<HTMLInputElement>document.getElementById(htmlid)).src="../assets/img/avatars/man.png";
        }

      }
    };

    xhr.send(null);
  }




  getArticleImage(idArt:number,htmlid:string){

    var xhr = new XMLHttpRequest();
    xhr.open("GET", environment.apiEndpointR+"articles/image/"+idArt , true);
    xhr.setRequestHeader("Authorization", localStorage.getItem("token"));
    xhr.responseType = "arraybuffer";
    xhr.onload = function (oEvent) {
      var arrayBuffer = xhr.response; // Note: not oReq.responseText
      if (arrayBuffer) {

        var u8 = new Uint8Array(arrayBuffer);
        var b64encoded = btoa(String.fromCharCode.apply(null, u8));
        console.log();
        if (u8.length){
          var mimetype = "image/png"; // or whatever your image mime type is childNodes[1].getAttribute('src')
          (<HTMLInputElement>document.getElementById(htmlid)).src ="data:" + mimetype + ";base64," + b64encoded;
        }
        else {
          (<HTMLInputElement>document.getElementById(htmlid)).src="../assets/img/avatars/diet1.png";
        }

      }
    };

    xhr.send(null);
  }

}

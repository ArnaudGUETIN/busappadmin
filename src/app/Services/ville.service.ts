import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
const endpoint = environment.endpoint;
@Injectable({
  providedIn: 'root'
})
export class VilleService {

  constructor(public http:HttpClient) { }

  getAllVille():any{
    return this.http.get(endpoint+"allVilles");
  }

  getOneVille(idVille:number){
    return this.http.get(endpoint+"ville/"+idVille);
  }
}

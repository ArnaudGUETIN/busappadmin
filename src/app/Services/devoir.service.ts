import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
const endpoint = "http://localhost:8080/devoirs/";
@Injectable({
  providedIn: 'root'
})
export class DevoirService {
  jwtToken:string;
  constructor(private http:HttpClient) {
    if(this.jwtToken==null) this.loadToken();
  }
  loadToken(){
    this.jwtToken=localStorage.getItem('token');
  }
  getDevoir(idDevoir:number):any{
    return this.http.get(endpoint+idDevoir,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  getAllDevoirsOfMatiere(idMatiere:number):any{
    return this.http.get(endpoint+"matiere/"+idMatiere+"/devoirs/all",{headers: new HttpHeaders({"Authorization":this.jwtToken})})
  }

  addDevoir(devoir:any):any{
    return this.http.post(endpoint+"add",devoir,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  addDevoirToMatiere(idDevoir:number,idMatiere:number,idClasse:number){
    return this.http.get(endpoint+"devoir/"+idDevoir+"/addTo/matiere/"+idMatiere+"/classe/"+idClasse,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
}

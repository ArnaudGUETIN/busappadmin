import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
const endpoint = "http://localhost:8080/shared/";
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  jwtToken:string;
  constructor(private http:HttpClient) {
    if(this.jwtToken==null) this.loadToken();
  }
  loadToken(){
    this.jwtToken=localStorage.getItem('token');
  }
  getMetrics(ecoleId:number):any{
    return this.http.get(endpoint+"ecole/"+ecoleId,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
const endpoint = "http://localhost:8080/anneeScolaires/";
@Injectable({
  providedIn: 'root'
})
export class AnneeScolaireService {
  jwtToken:string;
  constructor(private http:HttpClient) {
    if(this.jwtToken==null) this.loadToken();
  }

  loadToken(){
    this.jwtToken=localStorage.getItem('token');
  }
  getOneAnneeScolaire(idAnneeScolaire:number):any{
    if(this.jwtToken==null) this.loadToken();
    return this.http.get(endpoint+idAnneeScolaire,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }


  getAllAnneeScolaires():any{
    if(this.jwtToken==null) this.loadToken();
    return this.http.get(endpoint+"all",{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }


  addAnneeScolaire(anneeScolaire:any):any{
    if(this.jwtToken==null) this.loadToken();
    return this.http.post(endpoint+"add",anneeScolaire,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
  updateAnneeScolaire(anneeScolaire:any):any{
    if(this.jwtToken==null) this.loadToken();
    return this.http.put(endpoint+"update",anneeScolaire,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
  changeCurrentAnneeScolaire(idAnneeScolaire:number):any{
    if(this.jwtToken==null) this.loadToken();
    return this.http.get(endpoint+"updateCurrentAnneeScolaire/"+idAnneeScolaire,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
  deleteAnneeScolaire(idAnneeScolaire:number){
    if(this.jwtToken==null) this.loadToken();
    return this.http.delete(endpoint+"remove/"+idAnneeScolaire,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
}

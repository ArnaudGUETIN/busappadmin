import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Professor} from "../model/Professor";
const endpoint = "http://localhost:8080/professors/";
@Injectable({
  providedIn: 'root'
})
export class ProfessorService {
  jwtToken:string;
  constructor(private http:HttpClient) {
    if(this.jwtToken==null) this.loadToken();
  }
  loadToken(){
    this.jwtToken=localStorage.getItem('token');
  }
  getProfessor(idCours:number){
    return this.http.get(endpoint+idCours,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  getAllProfessorsOfScholl(idSchool:number):any{
    return this.http.get(endpoint+"school/"+idSchool+"/professors/all",{headers: new HttpHeaders({"Authorization":this.jwtToken})})
  }
  addProfessor(professor:Professor):any{
    return this.http.post(endpoint+"add",professor,{headers: new HttpHeaders({"Authorization":this.jwtToken})})
  }
  updateProfessor(professor:Professor){
    return this.http.put(endpoint+"update",professor,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  addProfessorToSchool(idProfessor:number,idSchool:number){
    return this.http.get(endpoint+idProfessor+"/addTo/school/"+idSchool,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  removeProfessor(professorId:number){
    return this.http.delete(endpoint+"remove/"+professorId,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
}

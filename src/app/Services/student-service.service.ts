import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Student} from "../model/Student";
import {Observable} from "rxjs/index";
const endpoint = "http://localhost:8080/students/";
@Injectable({
  providedIn: 'root'
})
export class StudentServiceService {
  jwtToken:string;
  constructor(private http:HttpClient) {
    if(this.jwtToken==null) this.loadToken();
  }
  loadToken(){
    this.jwtToken=localStorage.getItem('token');
  }
  getOneStudent(id:number):any{
    return this.http.get(endpoint+id,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
  getAllStudentsOfClasse(idClasse:number){
    return this.http.get<Student[]>(endpoint+"classe/"+idClasse+"/students/all",{headers: new HttpHeaders({"Authorization":this.jwtToken})})
  }
  getAllStudents(){
    return this.http.get<Student[]>(endpoint+"all",{headers: new HttpHeaders({"Authorization":this.jwtToken})})
  }
  addStudent(student:any){
    return this.http.post<Student>(endpoint+"add",student,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
  deleteStudent(idStudent:number){
    return this.http.delete(endpoint+"remove/"+idStudent,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
  update(student:any){
    return this.http.put<Student>(endpoint+"update",student,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  addStudentToClasse(idStudent:number,idClasse:number){
    return this.http.get<Student>(endpoint+idStudent+"/addTo/classe/"+idClasse,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  getCountries():any{
    var country_list = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas"
      ,"Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands"
      ,"Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica"
      ,"Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea"
      ,"Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana"
      ,"Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India"
      ,"Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia"
      ,"Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania"
      ,"Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia"
      ,"New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal"
      ,"Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles"
      ,"Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan"
      ,"Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia"
      ,"Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","United States Minor Outlying Islands","Uruguay"
      ,"Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];
    return country_list;
  }

  getMoyennesOfStudent(id:number):any{
    return this.http.get(endpoint+"moyennes/"+id,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
const endpoint = "http://localhost:8080/classes/";
@Injectable({
  providedIn: 'root'
})
export class ClasseServiceService {
  jwtToken:string;
  constructor(private http:HttpClient) {
    if(this.jwtToken==null) this.loadToken();
  }

  loadToken(){
    this.jwtToken=localStorage.getItem('token');
  }

  getOneClasse(idClasse:number):any{
    if(this.jwtToken==null) this.loadToken();
    return this.http.get(endpoint+idClasse,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  getAllClassesOfSchool(idSchool:number):any{
    if(this.jwtToken==null) this.loadToken();
    return this.http.get(endpoint+"school/"+idSchool+"/classes/all",{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  getAllClassesOfProfessor(idProfessor:number){
    if(this.jwtToken==null) this.loadToken();
    return this.http.get(endpoint+"professor/"+idProfessor+"/classes/all",{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }


  addClasse(classe:any):any{
    if(this.jwtToken==null) this.loadToken();
    return this.http.post(endpoint+"add",classe,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
  updateClasse(classe:any):any{
    if(this.jwtToken==null) this.loadToken();
    return this.http.put(endpoint+"update",classe,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
  deleteClasse(classeId:number){
    if(this.jwtToken==null) this.loadToken();
    return this.http.delete(endpoint+"remove/"+classeId,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  addClasseToSchool(classeId:number,schoolId:number){

    return this.http.get(endpoint+"school/"+schoolId+"/addTo/classe/"+classeId,{headers: new HttpHeaders({"Authorization":this.jwtToken})})
  }
}

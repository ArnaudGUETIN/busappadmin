import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
const endpoint = "http://localhost:8080/cours/";
@Injectable({
  providedIn: 'root'
})
export class CoursService {
  jwtToken:string;
  constructor(private http:HttpClient) {
    if(this.jwtToken==null) this.loadToken();
  }
  loadToken(){
    this.jwtToken=localStorage.getItem('token');
  }
  getCours(idCours:number){
    return this.http.get(endpoint+idCours);
  }

  getAllCoursOfClasse(idClasse:number):any{
    return this.http.get(endpoint+"classe/"+idClasse+"/cours/all",{headers: new HttpHeaders({"Authorization":this.jwtToken})})
  }

  addCours(cours:any):any{
    return this.http.post(endpoint+"add",cours,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  addCoursToClasse(idCours:number,idClasse:number){
    return this.http.get(endpoint+"classe/"+idClasse+"/addTo/cours/"+idCours,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  addCoursToMatiere(idCours:number,idMatiere:number){
    return this.http.get(endpoint+"matiere/"+idMatiere+"/addTo/cours/"+idCours,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }

  updateCours(cours:any){
    return this.http.put(endpoint+"update",cours,{headers: new HttpHeaders({"Authorization":this.jwtToken})});
  }
}

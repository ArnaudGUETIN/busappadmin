import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
const endpoint = environment.endpoint;
@Injectable({
  providedIn: 'root'
})
export class BusService {
  constructor(public http:HttpClient) { }
  getAllBus():any{
    return this.http.get(endpoint+"allBus");
  }

  getOneBus(idBus:number){
    return this.http.get(endpoint+"bus/"+idBus);
  }
}

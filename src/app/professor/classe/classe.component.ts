import { Component, OnInit } from '@angular/core';
import {ClasseServiceService} from "../../Services/classe-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-classe',
  templateUrl: './classe.component.html',
  styleUrls: ['./classe.component.scss']
})
export class ClasseComponent implements OnInit {
  classes:any=[];
  constructor(private classeService:ClasseServiceService,
              private  router:Router) { }

  ngOnInit() {
    this.classeService.getAllClassesOfProfessor(1)
      .subscribe(data=>{
       // console.log(data);
        this.classes=data;
      })
  }
  getClasse(classId:number){
   // console.log(classId);
    this.router.navigate(["professor/classeView",classId]);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClasseViewComponent } from './classe-view.component';

describe('ClasseViewComponent', () => {
  let component: ClasseViewComponent;
  let fixture: ComponentFixture<ClasseViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClasseViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClasseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

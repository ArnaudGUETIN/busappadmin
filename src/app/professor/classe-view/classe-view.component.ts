import { Component, OnInit } from '@angular/core';
import {StudentServiceService} from "../../Services/student-service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Student} from "../../model/Student";

@Component({
  selector: 'app-classe-view',
  templateUrl: './classe-view.component.html',
  styleUrls: ['./classe-view.component.scss']
})
export class ClasseViewComponent implements OnInit {

  idClasse:number;
  displayDialog:boolean=false;
  displayEventDialog:boolean=false;
  selectedStudent:Student = new Student();
  date:Date=new Date();
  students:Student[]=[];
  student:Student=new Student();
  cols:any=[];
  events: any[];
  options: any;
  constructor(private route:ActivatedRoute,
              private router:Router,
              private studentService:StudentServiceService) { }

  ngOnInit() {
    this.idClasse=+this.route.snapshot.params['id'];
    this.cols = [
      { field: 'name', header: 'Nom' },
      { field: 'surname', header: 'Prénom' },
      { field: 'email', header: 'Email' },
      { field: 'moyenne', header: 'Moyenne' },
      { field: 'rang', header: 'Rang' }
    ];

    this.studentService.getAllStudentsOfClasse(this.idClasse)
      .subscribe((data:Student[])=>{
        this.students=data;
       // console.log(this.students);
      })


    this.events = [
      {
        "title": "All Day Event",
        "start": "2019-01-01"
      },
      {
        "title": "Long Event",
        "start": "2019-01-07",
        "end": "2019-01-10",
        "color":'purple'
      },
      {
        "title": "Repeating Event",
        "start": "2019-01-09T16:00:00",
        "color":'purple'
      },
      {
        "title": "Repeating Event",
        "start": "2019-01-16T16:00:00",
        "color":'purple'
      },
      {
        "title": "Conference",
        "start": "2019-01-11",
        "end": "2019-01-13",
        "color": '#378006'
      }
    ];
    this.options = {
      defaultDate: new Date(),
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      editable: true
    };
  }

  showDialogToAdd() {
    this.student=new Student;
    this.displayDialog = true;
  }

   onRowSelect(event) {
    this.student = this.cloneCar(event.data);
    //console.log(event.data);
    this.displayDialog = true;
  }

  cloneCar(student: Student): Student {
    let s = new Student();
    for (let prop in student) {
      s[prop] = student[prop];
    }
    return s;
  }
  save(){
    this.studentService.addStudent(this.student)
      .subscribe((data:Student)=>{
        this.student=data;
        this.studentService.addStudentToClasse(this.student.studentId,this.idClasse)
          .subscribe(data=>{
            this.students.push(data);
          })
      });
    this.ngOnInit();
  }
  saveEvent(){
    this.events.push({
      "title": "Test",
      "start": this.date,
      "color": '#378006'
    });
   //console.log(this.events);
    //this.ngOnInit();
  }
  showEventDialogToAdd() {
    this.date=new Date();
    this.displayEventDialog = true;
  }
}

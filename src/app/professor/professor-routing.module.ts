import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StudentComponent} from "./student/student.component";
import {BulletinComponent} from "./bulletin/bulletin.component";
import {ProgrammeComponent} from "./programme/programme.component";
import {NoteComponent} from "./note/note.component";
import {ProfessorComponent} from "./professor/professor.component";
import {ClasseComponent} from "./classe/classe.component";
import {ClasseViewComponent} from "./classe-view/classe-view.component";

const routes: Routes = [
    {
        path: 'professor',
        data: {
            title: 'Professor'
        },

        children: [
            {
                path: 'students',
                component: StudentComponent,
                data: {
                    title: 'Student'
                }
            },
            {
                path: 'bulletins',
                component: BulletinComponent,
                data: {
                    title: 'Bulletins'
                }
            },
            {
                path: 'notes',
                component: NoteComponent,
                data: {
                    title: 'Notes'
                }
            },
            {
                path: 'programmes',
                component: ProgrammeComponent,
                data:{
                  title:'Programmes'
                }
            },
          {
            path: 'details',
            component: ProfessorComponent,
            data:{
              title:'Details'
            }
          },
          {
            path: 'classes',
            component: ClasseComponent,
            data:{
              title:'Classes'
            }
          },
          {
            path: 'classeView/:id',
            component: ClasseViewComponent,
            data: {
              title: 'Classe'
            }
          },
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfessorRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfessorRoutingModule } from './professor-routing.module';
import {StudentComponent} from "./student/student.component";
import {BulletinComponent} from "./bulletin/bulletin.component";
import {ProgrammeComponent} from "./programme/programme.component";
import {NoteComponent} from "./note/note.component";
import { ProfessorComponent } from './professor/professor.component';
import { ClasseComponent } from './classe/classe.component';
import { ClasseViewComponent } from './classe-view/classe-view.component';
import {FullCalendarModule} from "primeng/fullcalendar";
import {AccordionModule, DataTableModule, DropdownModule, MultiSelectModule} from "primeng/primeng";
import {FormsModule} from "@angular/forms";
import {DialogModule} from "primeng/dialog";
import {DataViewModule} from "primeng/dataview";
import {TableModule} from "primeng/table";
import {ButtonModule} from "primeng/button";

@NgModule({
  declarations: [StudentComponent, BulletinComponent, NoteComponent, ProgrammeComponent, ProfessorComponent, ClasseComponent, ClasseViewComponent],
  imports: [
    CommonModule,
    ProfessorRoutingModule,
    DataTableModule,
    ButtonModule,
    TableModule,
    DataViewModule,
    DropdownModule,
    MultiSelectModule,
    DialogModule,
    FormsModule,
    AccordionModule,
    FullCalendarModule
  ]
})
export class ProfessorModule { }
